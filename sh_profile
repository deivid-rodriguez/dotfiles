#!/usr/bin/env sh

#
# If not running interactively, don't do anything
#
[ -z "$PS1" ] && return

custom_folder="$HOME/.profile.d"

#
# General utilities
#

load_general_utils() {
  # shellcheck source=profile.d/auth
  . "$custom_folder/auth"

  # shellcheck source=profile.d/pager
  . "$custom_folder/pager"

  # shellcheck source=profile.d/editor
  . "$custom_folder/editor"

  # shellcheck source=profile.d/paths
  . "$custom_folder/paths"

  # shellcheck source=profile.d/brew
  . "$custom_folder/brew"

  # shellcheck source=profile.d/env
  . "$custom_folder/env"

  # shellcheck source=profile.d/utils
  . "$custom_folder/utils"

  if [ -f "$custom_folder/prompt" ]
  then
    # shellcheck source=/dev/null
    . "$custom_folder/prompt"
  fi

  # shellcheck source=profile.d/media
  . "$custom_folder/media"

  # shellcheck source=profile.d/docker
  . "$custom_folder/docker"

  # shellcheck source=profile.d/git
  . "$custom_folder/git"

  # shellcheck source=profile.d/go
  . "$custom_folder/go"

  # shellcheck source=profile.d/pg
  . "$custom_folder/pg"

  # shellcheck source=profile.d/snap
  . "$custom_folder/snap"
}

load_general_utils

unset load_general_utils

#
# Folder for local bins
#
PATH_append "$HOME/.local/bin"

load_language_customizations() {
  # shellcheck source=profile.d/c
  . "$custom_folder/c"

  # shellcheck source=profile.d/cargo
  . "$custom_folder/cargo"

  # shellcheck source=profile.d/java
  . "$custom_folder/java"

  # shellcheck source=profile.d/js
  . "$custom_folder/js"

  # shellcheck source=profile.d/latex
  . "$custom_folder/latex"

  # shellcheck source=profile.d/rails
  . "$custom_folder/rails"

  # shellcheck source=profile.d/ruby
  . "$custom_folder/ruby"
}

load_language_customizations

unset load_language_customizations

# shellcheck source=bash_aliases
. "$HOME/.bash_aliases"

# shellcheck source=profile.d/projects
. "$custom_folder/projects"
