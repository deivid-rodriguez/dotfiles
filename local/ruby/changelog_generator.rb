# frozen_string_literal: true

require "open3"
require "yaml"

gem_name = ARGV[0]

output, status = Open3.capture2e("gem list i18n-spec --all --remote")
abort unless status.success?

all_versions = output.match(/\((.*)\)/)[1].split(",").map(&:strip).sort do |v1, v2|
  v1_major, v1_minor, v1_patch = v1.split(".").map(&:to_i)
  v2_major, v2_minor, v2_patch = v2.split(".").map(&:to_i)

  if v1_major == v2_major
    if v1_minor == v2_minor
      v2_patch <=> v1_patch
    else
      v2_minor <=> v1_minor
    end
  else
    v2_major <=> v1_major
  end
end

specifications = all_versions.map do |version|
  output, status = Open3.capture2e("gem specification #{gem_name} --version #{version} --remote")
  abort unless status.success?

  YAML.load(output) # rubocop:disable Security/YAMLLoad
end

File.open("CHANGELOG.md", "w") do |f|
  f.puts("# Changelog")
  f.puts
  f.puts("## [Unreleased]")
  f.puts

  specifications.each.with_index do |specification, index|
    date = specification.date.strftime("%Y-%d-%m")
    version = specification.version

    if index == specifications.size - 1
      f.puts("## #{version} - #{date}")
    else
      f.puts("## [#{version}] - #{date}")
    end

    f.puts
  end

  repo = specifications.first.homepage

  if repo.start_with?("http:")
    warn "Warning: Repo's gemspec uses non-secue http protocol"
    repo = repo.gsub(/^http:/, "https:")
  end

  base_comparison = "#{repo}/compare"

  f.puts("[Unreleased]: #{base_comparison}/v#{specifications.first.version}..HEAD")

  specifications[0..-2].each.with_index do |specification, index|
    version = specification.version
    previous_version = specifications[index + 1].version

    f.puts("[#{version}]: #{base_comparison}/v#{previous_version}..v#{version}")
  end
end
