#!/usr/bin/env bash

#
# Reversed cat
#
if [ "$(uname)" = "Darwin" ]
then
  tac() {
    tail -r
  }
fi

#
# Flat list of references from git log, excluding tags
#
git_ref_list ()
{
  git log --pretty=format:"%D" | awk -F", " '{ for(i = 1; i <= NF; ++i) print $i }' | grep -v tag: | grep -v "\->"
}

#
# Finds the head commit of a branch
#
last_commit_from ()
{
  git show --no-patch --pretty=format:%h "$1"
}

#
# Finds the base branch for the current branch, using the passed remote.
#
# $1: The remote to be used
#
base_for ()
{
  local_base=$(git_ref_list | grep "$1" | head -1)

  if [ "$local_base" != "" ]
  then
    last_head_commit=$(last_commit_from HEAD)
    last_local_commit=$(last_commit_from "$local_base")

    if [ "$last_head_commit" = "$last_local_commit" ]
    then
      local_base=$(git_ref_list | grep "$1" | head -2 | tail -1)
    fi
  fi

  echo "$local_base"
}

#
# Review every commit on a branch, oldest to newest
#
upstream_base=$(base_for "upstream")
origin_base=$(base_for "origin")

if [ "$upstream_base" = "" ]
then
  base=$origin_base
else
  base=$upstream_base
fi

revisions=$(git log "$base...HEAD" --pretty=format:%h ; echo)
revisions=$(echo "$revisions" | tac)
mapfile -t revisions <<< "$revisions"

git show "${revisions[@]}"
