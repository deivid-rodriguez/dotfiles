#!/usr/bin/env sh

download_it() {
  if [ ! -f profile.d/prompt ]
  then
    curl -sSL https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh > profile.d/prompt
  fi

  if [ ! -f completion.d/bash/git-base ]
  then
    curl -sSL https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash > completion.d/bash/git-base
  fi

  if [ ! -f completion.d/bash/fzf ]
  then
    curl -sSL https://raw.githubusercontent.com/junegunn/fzf/80b5bc1b682898e3089f6caf1120919fc0933562/shell/completion.bash > completion.d/bash/fzf
  fi

  git submodule update --init
}

sync_it() {
  os="$(uname | tr '[:upper:]' '[:lower:]')"

  rsync -ah --delete profile.d/ ~/.profile.d
  rsync -ah --delete completion.d/ ~/.completion.d
  rsync -adh bundle/ ~/.bundle

  rsync -ah --delete "config.$os/git/config" ~/.config/git/config
  rsync -adh config/ ~/.config
  chmod 700 ~/.config/op

  rsync -ah local/ ~/.local

  mkdir -p ~/.ssh
  rsync -ah --delete "ssh/config.$os" ~/.ssh/config

  if command -v brew > /dev/null
  then
    git_zsh_completions="/usr/share/zsh/$ZSH_VERSION/functions/_git"
    brew_prefix="$(brew --prefix)"

    [ -f "$git_zsh_completions" ] && ln -sf "$git_zsh_completions" "$brew_prefix/share/zsh/site-functions/_git"
    ln -sf ~/.completion.d/zsh/_git-edit "$brew_prefix/share/zsh/site-functions/_git-edit"
    ln -sf ~/.completion.d/zsh/_git-finish "$brew_prefix/share/zsh/site-functions/_git-finish"
    ln -sf ~/.completion.d/zsh/_git-review "$brew_prefix/share/zsh/site-functions/_git-review"
    ln -sf ~/.completion.d/zsh/_git-start "$brew_prefix/share/zsh/site-functions/_git-start"
  fi

  sync_file ignore
  sync_file inputrc
  sync_file bash_completion
  sync_file zsh_completion
  sync_file profile
  sync_file bash_aliases
  sync_file pryrc
  sync_file byebugrc
  sync_file gemrc
  sync_file bash_profile
  sync_file bashrc
  sync_file sh_profile
  sync_file tool-versions
  sync_file zsh_profile
  sync_file zshrc
}

sync_file() {
  rsync -ah --delete "$1" "$HOME/.$1"
}

source_it() {
  exec "$SHELL"
}

download_it
sync_it
source_it

unset download_it
unset sync_it
unset source_it
