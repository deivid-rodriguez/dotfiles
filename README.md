# My dotfiles

Install them with

```
./bootstrap.sh download
```

First installation needs to download some files, but for subsequent reinstalls

```
./bootstrap.sh
```

should suffice.
