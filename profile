#!/bin/sh

# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login exists.

#
# Load all bash-specific stuff if running bash
#
if [ -n "$BASH_VERSION" ]
then
  # shellcheck source=bash_profile
  [ -f "$HOME/.bash_profile" ] && . "$HOME/.bash_profile"
fi

#
# Load all zsh-specific stuff if running zsh
#
if [ -n "$ZSH_VERSION" ]
then
  # shellcheck disable=SC1091
  [ -f "$HOME/.zsh_profile" ] && . "$HOME/.zsh_profile"
fi
