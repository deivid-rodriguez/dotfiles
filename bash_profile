#!/usr/bin/env bash

# shellcheck source=sh_profile
source "$HOME/.sh_profile"

#
# Command history
#

#
# Don't put lines matching the last history entry or lines starting with space
# in the command history. Also, erase duplicates before saving new entries
#
HISTCONTROL=ignoreboth:erasedups

#
# Append to the history file, don't overwrite it
#
shopt -s histappend

#
# Set unlimited history length
#
HISTSIZE=
HISTFILESIZE=

#
# Ignore very short commands
#
HISTIGNORE="?:??:???"

#
# Set default terminal
#
export TERM=xterm-256color

#
# Set default bat them
#
export BAT_THEME=GitHub

#
# Set default language
#
export LANGUAGE=C.utf8

#
# Default bc configuration
#
export BC_ENV_ARGS="$HOME/.config/bc"

#
# Check the window size after each command and, if necessary, update the values
# of LINES and COLUMNS.
#
shopt -s checkwinsize

#
# Enable the "**" pattern used in a pathname expansion context to match all
# files and zero or more directories and subdirectories. NOTE: Requires bash 4
#
shopt -s globstar

#
# Make less more friendly for non-text input files, see lesspipe(1)
#
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# shellcheck source=bash_completion
source "$HOME/.bash_completion"

#
# Set some color variables
#
turquoise='\[\033[49;36m\]'
blue='\[\033[01;34m\]'
yellow='\[\033[33m\]'
reset='\[\033[00m\]'

#
# Set custom prompt
#
PS1="$yellow\$(__git_ps1 '(%s) ')$turquoise\\u@\\h$blue \\w \\$ $reset"
